Vue.component('cat-card',
{
    props:["img", "name", "breed"],
    template:
    `<div class="card">
    <div class="image-container">
        <img class="cat-img" :src="img" />
    </div>
    <span class="cat-name">{{ name }}</span>
    <span class="cat-breed">{{ breed }}</span>
</div>`
})

const app = new Vue({
    el:'#app',
    data:
    {
        catList: []
    },
    mounted: async function()
    {
        const data = await fetch("http://localhost:3000/api/cats")
        // Here we convert the data in a json format
        const arrOfCats = await data.json()

        this.catList = arrOfCats;
    }
})